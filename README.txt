TurboKid
========

This package provides a template engine plugin, allowing you
to easily use Kid with TurboGears, Buffet or other systems
that support python.templating.engines.

There are plans to integrate this functionality into Kid,
so this package may eventually become superfluous.

Kid templates are assumed to have a "kid" extension.

For information on the Kid templating engine, go here:

http://kid-templating.org

Kid is the standard templating engine for TurboGears 1.0, so
for information on using Kid templates with TurboGears, go here:

http://docs.turbogears.org/1.0/GettingStarted/Kid

For general information on using a template engine plugin
with TurboGears or writing a template engine plugin, go here:

http://docs.turbogears.org/1.0/TemplatePlugins
http://docs.turbogears.org/1.0/AlternativeTemplating
