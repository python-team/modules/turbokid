turbokid (1.0.5-3) UNRELEASED; urgency=medium

  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/changelog: Remove trailing whitespaces
  * d/control: Remove ancient X-Python-Version field
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Ondřej Nový <novy@ondrej.org>  Tue, 29 Mar 2016 22:36:03 +0200

turbokid (1.0.5-2) unstable; urgency=low

  * Team upload

  [ SVN-Git Migration ]
  * git-dpm config.
  * Update Vcs fields for git migration.

  [ Mattia Rizzolo ]
  * Use source format 3.0 (quilt).
  * Remove long useless debian/pycompat file.
  * Build using dh-python instead of python-support.  Closes: #786267
  * Run wrap-and-sort.
  * Remove duplicated python build-dep.
  * Bump debian/compat to 9.
  * Bump Standards-Version to 3.9.6: no changes needed.
  * Rewrite debian/watch using the pypi.debian.net redirector.

 -- Mattia Rizzolo <mattia@debian.org>  Tue, 15 Dec 2015 01:41:17 +0000

turbokid (1.0.5-1) unstable; urgency=low

  * New upstream release.

 -- Federico Ceratto <federico.ceratto@gmail.com>  Tue, 06 Apr 2010 20:59:08 +0100

turbokid (1.0.4-6) unstable; urgency=low

  * New mantainer. (Closes: #544634).
  * debian/control: Vcs switched to svn.debian.org
  * debian/watch added
  * debian/rules rebuilt for dh7
  * quilt-based patch removed

 -- Federico Ceratto <federico.ceratto@gmail.com>  Thu, 22 Oct 2009 22:04:48 +0100

turbokid (1.0.4-5) unstable; urgency=low

  * Orphaning package.

 -- Daniel Baumann <daniel@debian.org>  Tue, 16 Jun 2009 08:15:00 +0200

turbokid (1.0.4-4) unstable; urgency=low

  * New maintainer (Closes: #507791).
  * Redone debian packaging from scratch.

 -- Daniel Baumann <daniel@debian.org>  Sun, 11 Jan 2009 15:26:00 -0500

turbokid (1.0.4-3) unstable; urgency=low

  [ Sandro Tosi ]
  * debian/control
    - switch Vcs-Browser field to viewsvn

  [ Gustavo Noronha Silva ]
  * Orphaning the package.

 -- Gustavo Noronha Silva <kov@debian.org>  Thu, 04 Dec 2008 11:41:37 -0200

turbokid (1.0.4-2) unstable; urgency=low

  * debian/control:
  - replaced python-setuptools runtime dependency with a
    python-pkg-resources one (Closes: #468707)

 -- Gustavo Noronha Silva <kov@debian.org>  Sat, 10 May 2008 09:36:29 -0300

turbokid (1.0.4-1) unstable; urgency=low

  [ Piotr Ożarowski ]
  * New python-support handles egg's directory name correctly
    - bump python-support required version
    - remove mv part from debian/rules
  * Vcs-Browser and Homepage fields added
  * XS-Vcs-Svn field renamed to Vcs-Svn

  [ Sandro Tosi ]
  * debian/control
    - uniforming Vcs-Browser field

  [ Gustavo Noronha Silva ]
  * New upstream release
  * debian/patches/01_disable_requirements.diff:
  - updated
  * debian/control:
  - remove -1 from setuptools' version requirement on build-deps
  - updated Standards-Version to 3.7.3

 -- Gustavo Noronha Silva <kov@debian.org>  Sun, 10 Feb 2008 23:11:06 -0200

turbokid (1.0.1-1) unstable; urgency=low

  [ Piotr Ozarowski ]
  * Added XS-Vcs-Svn field

  [ Gustavo Noronha Silva ]
  * New upstream version
  * debian/control:
  - updated version requirements for python-support and move it
    and python-all-dev to Build-Deps, since they are needed to
    run clean target
  - updated Depends on python-kid to >= 0.9.5
  * debian/patches/01_disable_requirements.diff:
  - update to cope with changes in the upstream file
  * debian/watch:
  - added to watch for new versions automatically
  * debian/pycompat:
  - removed; no longer used, since dh_python was dropped

 -- Gustavo Noronha Silva <kov@debian.org>  Sat, 12 May 2007 15:06:21 -0300

turbokid (0.9.9-1) unstable; urgency=low

  * New upstream release
  * debian/patches/01_disable_requirements.diff:
  - updated

 -- Gustavo Noronha Silva <kov@debian.org>  Mon, 20 Nov 2006 23:13:42 -0200

turbokid (0.9.8-1) unstable; urgency=low

  * New upstream release
  * debian/control:
  - require newer versions of cdbs and debhelper which respect the
    2.4 requirement when calling setup.py
  * debian/rules:
  - use DEB_UPSTREAM_VERSION, and shell globbing instead of detecting the
    python version when renaming the egg-info directory
  (Closes: #379547)
  * debian/patches/01_disable_requirements.diff:
  - added; disable the kid >= 0.9.3 requirement; Debian has only
    0.9.1, which works, and I can handle the "I don't want kid
    0.9.2" requirement as a Conflict, if needed (but not for now)

 -- Gustavo Noronha Silva <kov@debian.org>  Mon, 24 Jul 2006 21:28:45 -0300

turbokid (0.9.6-3) unstable; urgency=low

  * debian/control, debian/pyversions, debian/pycompat:
  - use python-support 0.3
  * debian/python-support.version:
  - removed
  * debian/rules:
  - rename egg-info directory to remove the python version

 -- Gustavo Noronha Silva <kov@debian.org>  Fri, 23 Jun 2006 16:55:57 -0300

turbokid (0.9.6-2) experimental; urgency=low

  * debian/control, debian/rules:
  - support the new Python Policy
  * debina/python-turbokid.{postinst,prerm}:
  - removed; now generated by dh_pysupport

 -- Gustavo Noronha Silva <kov@debian.org>  Sat, 20 May 2006 14:23:54 -0300

turbokid (0.9.6-1) experimental; urgency=low

  * Initial release (Closes: #367283)

 -- Gustavo Noronha Silva <kov@debian.org>  Sat, 13 May 2006 18:59:40 -0300
